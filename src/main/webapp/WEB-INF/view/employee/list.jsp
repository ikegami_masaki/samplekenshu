<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><c:out value="${ title }" /></title>
</head>
<body>
	<h1>
		<c:out value="${ title }" />
	</h1>

	<p>
		<c:out value="${ message }" />
	</p>
	<form:form modelAttribute="employeeListForm">
		<table>
			<tbody>
				<tr>
					<td><form:label path="name">社員名</form:label></td>
					<td><form:input path="name" size="20" /></td>
				</tr>
				<tr>
					<td><form:label path="age">年齢</form:label></td>
					<td><form:input path="age" size="20" /></td>
				</tr>
				<tr>
					<td><form:label path="memo">メモ</form:label></td>
					<td><form:textarea path="memo" cols="20" row="5" /></td>
				</tr>
			</tbody>
		</table>
		<input type="submit">
	</form:form>

	<c:if test="${ not empty employeeList }">
		<table border="1">
			<tbody>
				<c:forEach var="employee" items="${ employeeList }">
					<tr>
						<th><c:out value="${ employee.name }" /></th>
						<th><c:out value="${ employee.age }" /></th>
						<th><c:out value="${ employee.memo }" /></th>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if>

</body>
</html>