package jp.co.kenshu.controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.form.RadioForm;

@Controller
public class RadioSampleController {

	@RequestMapping(value = "/sample/radio/disp", method = RequestMethod.GET)
	public String disp(Model model) {
		// messageを設定
		model.addAttribute("message", "radioのサンプル");
		// フォームの初期値を設定
		RadioForm form = new RadioForm();
		form.setName("次郎");
		model.addAttribute("radioForm", form);
		// 従業員リストを作成する
		model.addAttribute("checkEmployees", getRadioEmployees());
		return "radioSample";
	}

	@RequestMapping(value = "/sample/radio/info", method = RequestMethod.POST)
	public String getRadioInfo(@ModelAttribute RadioForm form, Model model) {
		// フォームから値を取得
		String selectedName = form.getName();
		// 選択された名前を表示
		model.addAttribute("message", selectedName);
		model.addAttribute("radioForm", form);
		// 従業員リストをセット
		model.addAttribute("checkEmployees", getRadioEmployees());
		return "radioSample";
	}

	/**
	 * @return 従業員のリスト
	 * */
	private List<String> getRadioEmployees(){
		List<String> list = new LinkedList<String>();
		list.add("一太郎");
		list.add("次郎");
		list.add("三郎");
		return list;
	}
}
